package com.bra.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectMilllstonesServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectMilllstonesServiceApplication.class, args);
    }

}
